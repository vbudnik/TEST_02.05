/*
// FUNCTION IN THE END OF FILE
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum
{
	false,
	true
}	boolean;

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*res;
	size_t	i;

	i = 0;
	if (s == NULL)
		return (NULL);
	res = (char *)malloc(sizeof(char) * (len + 1));
	if (res == NULL)
		return (NULL);
	while (len--)
	{
		res[i] = s[start + i];
		i++;
	}
	res[i] = '\0';
	return (res);
}

static int	ft_count_substr(const char *s, char c)
{
	int		count;
	int		substring;

	substring = 0;
	count = 0;
	while (*s != '\0')
	{
		if (substring == 1 && *s == c)
			substring = 0;
		if (substring == 0 && *s != c)
		{
			substring = 1;
			count++;
		}
		s++;
	}
	return (count);
}

static int	ft_lenstr(const char *s, char c)
{
	int	len;

	len = 0;
	while (*s != c && *s != '\0')
	{
		len++;
		s++;
	}
	return (len);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**t;
	int		nb_word;
	int		index;

	if (s == NULL)
		return (NULL);
	index = 0;
	nb_word = ft_count_substr((const char *)s, c);
	t = (char **)malloc(sizeof(*t) * (ft_count_substr((const char *)s, c) + 1));
	if (t == NULL)
		return (NULL);
	while (nb_word--)
	{
		while (*s == c && *s != '\0')
			s++;
		t[index] = ft_strsub((const char *)s, 0, ft_lenstr((const char *)s, c));
		if (t[index] == NULL)
			return (NULL);
		s = s + ft_lenstr(s, c);
		index++;
	}
	t[index] = NULL;
	return (t);
}

char		*f_skle(char **split, int size)
{
	int		i;
	int		j;
	int		len;
	int		k;
	char	*res;

	i = 0;
	len = 5;
	j = size;
	k = 0;
	size--;
	
	if (!(res = (char *)malloc(sizeof(char) * (len))))
		return (NULL);
	size = 0;
	while(split[size] != NULL)
	{
		i = 0;
		while (split[size][i] != '\0')
		{
			res[k] = split[size][i];
			k++;
			i++;
		}
		size++;
	}
	res[k] = '\0';
	return(res);
}

int		count_size(char **split)
{
	int		i;

	i = 0;
	while(split[i] != NULL)
		i++;
	return (i);
}

/*
//FUNCTION )))
*/

boolean IsBetween (char* Time, char* Start, char* End)
{
	char	**s1;
	char	**s2;
	char	**s3;
	char	*t;
	char	*s;
	char	*e;
	int		tim;
	int		sta;
	int 	en;

	s1 = ft_strsplit(Time, ':');
	s2 = ft_strsplit(Start, ':');
	s3 = ft_strsplit(End, ':');
	t = f_skle(s1, count_size(s1));
	s = f_skle(s2, count_size(s2));
	e = f_skle(s3, count_size(s3));
	tim = atoi(t);
	sta = atoi(s);
	en = atoi(e);
	if (sta < en)
		if (sta <= tim && en >= tim)
			return (true);
		else
			return (false);
	else if (sta > en)
		if (sta >= tim && en <= tim)
			return (true);
		else
			return (false);
	else
		return (false);
}