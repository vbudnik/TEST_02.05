#include <stdio.h>
#include <stdlib.h>

typedef struct s_node
{
  char*				val;
  struct s_node		*left;
  struct s_node		*right;
}					t_node;

int		count_level(t_node *current)
{
	int		left_level;
	int		right_leve;

	if (current == NULL)
		return 0;
	left_level = 1 + count_level(current->left);
	right_leve = 1 + count_level(current->right);
    if (left_level > right_leve)
		return (left_level);
	else
		return (right_leve);
}

void	printlevel(t_node *current, int level)
{
	if (current != NULL && level == 0)
		printf("%s ", current->val);
	else if (current != NULL)
	{
		printlevel(current->left, level - 1);
		printlevel(current->right, level - 1);
    }
}

void print(t_node *current) // function for print
{
    int i;
    int level;

	level = count_level(current);
	i = 0;
	while (i < level)
        printlevel(current, i++);
}


/*
// Sorry, but printing all values by space not new line (
*/
