#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

static int	count_words(char *str)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (str && str[i])
	{
		while (str[i] && str[i] == ' ')
			i++;
		if (str[i])
		{
			count++;
			while (str[i] && str[i] != ' ')
				i++;
		}
	}
	return (count);
}

char	**ft_split(char *str)
{
	char	**split;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!(split = (char **)malloc(sizeof(char *) * (count_words(str) + 1))))
		return (split);
	while (str && str[i])
	{
		while (*str && (*str == ' '))
			str++;
		if (*str)
		{
			while (str[i] && str[i] != ' ')
				i++;
			split[j] = strndup(str, i);
			j++;
			str = str + i;
			i = 0;
		}
	}
	split[j] = NULL;
	return (split);
}

int		count_size(char **split)
{
	int		i;

	i = 0;
	while(split[i] != NULL)
		i++;
	return (i);
}

char	*f_tolow(char *str)
{
	int		i;
	int		len;
	char	*res;

	i = 0;
	len = strlen((char*)str);
	res = (char *)malloc(sizeof(char) * len);
	while (str[i] != '\0')
	{
		if (str[i] >= 'A' && str[i] <= 'Z')
			res[i] = str[i] + 32;
		else
			res[i] = str[i];
		i++;
	}
	res[len] = '\0';
	return (res);
}

void	sort(char **str, int size)
{
	int		i;
	int		j;
	char	*tmp1;
	char	*tmp2;
	char	*tmp;

	i = 0;
	while (i < size)
	{
		j = i;
		while(j < size - 1)
		{
			tmp1 = f_tolow(str[j]);
			tmp2 = f_tolow(str[j + 1]);
			if (strcmp(tmp1, tmp2) > 0)
			{
				tmp = str[j];
				str[j] = str[j + 1];
				str[j + 1] = tmp;
			}
			j++;
		}
		i++;
	}
}

char	*f_skle(char **split, int size)
{
	int		i;
	int		j;
	int		len;
	int		k;
	char	*res;

	i = 0;
	len = 0;
	j = size;
	k = 0;
	size--;
	while (split[size])
		len += strlen(split[size--]);
	if (!(res = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	size = 0;
	while(split[size] != NULL)
	{
		i = 0;
		while (split[size][i] != '\0')
		{
			res[k] = split[size][i];
			k++;
			i++;
		}
		res[k] = ' ';
		k++;
		size++;
	}
	res[k] = '\0';
	return(res);
}

char	*sort_string(char *str)
{
	char	**split;
	int		size;
	char	*res;

	split = ft_split(str);
	size = count_size(split);
	sort(split, size);
	res = f_skle(split, size);
	return (res);
}

int main(int ac, char **av)
{
	char *str = "BBELLLL adfa GGHHHJJK KKKKH GGGG";
	char *s2;


	s2 = sort_string(str);
	printf("%s\n", s2);
	return (0);
}
