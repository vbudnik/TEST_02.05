/*
// FUNCTION IN THE END OF FILE
*/

#include <sys/stat.h>
#include <dirent.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define SLAH "/"
#define MAX_NAME 1024

struct dirent	*dp;
struct stat		sb;

typedef struct	s_opt
{
	char		*res_name;
	char		***all_mass;
	char		*pattern;
	char		*name_file;
}				t_opt;

int		f_rec_find(char *name_dir, char ***mass, int i, t_opt *res);

void	ft_memdel(void **ap)
{
	if (*ap && ap)
	{
		free(*ap);
		*ap = NULL;
	}
}

void	ft_strdel(char **as)
{
	if (as != NULL && *as != NULL)
		ft_memdel((void **)as);
}

char	*ft_strnew(size_t size)
{
	char	*res;

	res = (char *)malloc(size + 1);
	if (res == NULL)
		return (NULL);
	memset(res, '\0', size + 1);
	return (res);
}

char	*ft_strjoin(char *s1, char *s2)
{
	char	*res;

	if (!s1 && !s2)
		return (NULL);
	if (!s1)
		return (strdup(s2));
	if (!s2)
		return (strdup(s1));
	if (!(res = ft_strnew(strlen(s1) + strlen(s2))))
		return (NULL);
	strcpy(res, s1);
	strcat(res, s2);
	return (res);
}


void	f_free_not(char **del, char ***mass)
{
	free(del);
	free(mass);
}

int		f_free_mass(int l, char **mass)
{
	int		i;

	i = 0;
	while (i < l)
	{
		ft_strdel(&mass[i]);
		i++;
	}
	free(mass);
	return (0);
}

int		f_count_all(char *name_dir)
{
	DIR		*dir;
	int		i;

	i = 0;
	if (!(dir = opendir(name_dir)))
		return (0);
	while ((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
			continue ;
		i++;
	}
	return (i);
}

void	f_read_dir(DIR *dir, char **mass, char *dir_name)
{
	char	tmp[MAX_NAME];
	char	*del;

	while((dp = readdir(dir)) != NULL)
	{
		*mass = (char *)malloc(sizeof(char) * strlen(dp->d_name));
		if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
			*mass = strdup(dp->d_name);
		else
		{
			strcpy(tmp, dir_name);
			strcat(tmp, SLAH);
			strcat(tmp, dp->d_name);
			del = *mass;
			*mass = ft_strjoin(*mass, tmp);
			ft_strdel(&del);
		}
		bzero(tmp, strlen(tmp));
		mass++;
	}
	*mass = NULL;
}

char	*f_name(char *file_name, char *way)
{
	int		i;
	int		j;
	char	*str;

	i = 0;
	j = 0;
	if (!(str = (char *)malloc(sizeof(char) * (strlen(file_name) - strlen(way) + 3))))
		return (NULL);
	while (file_name[i] == way[i])
		i++;
	i++;
	while (file_name[i] != '\0')
	{
		str[j] = file_name[i];
		i++;
		j++;
	}
	str[j] = '\0';
	return (str);
}

int		f_cheak_file(char **arr, t_opt *res, int size, char *name_dir)
{
	int			fd;
	int			i;
	int			j;
	char		buff[1];
	static char	*str;
	char		*name;

	j = 2;
	while (j < size + 1)
	{
		stat(arr[j], &sb);
		name = f_name(arr[j], name_dir);
		if (S_ISREG(sb.st_mode) && strcmp(res->name_file, name) == 0)
		{
			i = 0;
			str = (char *)malloc(sizeof(char) * 10000);
			str = realloc(str, 10000);
			if (!(fd = open(arr[j], O_RDONLY)))
				return (0);
			while (read(fd, &buff, 1))
				str[i++] = buff[0];
			close(fd);
			if (strstr(str, res->pattern))
			{
				res->res_name = strdup(arr[j]);
				break ;
			}
		}
		j++;
	}
	if (res->res_name == NULL)
		return (0);
	else
		return (0);
}

int		f_rec_count(char **mass, char ***rec, int i, t_opt *res)
{
	int		j;

	j = 2;
	while(mass[j])
	{
		stat(mass[j], &sb);

		if (S_ISDIR(sb.st_mode))
		{
			i++;
			i = f_rec_find(mass[j], rec, i, res);
		}
		j++;
	}
	return (i);
}

int		f_rec_find(char *name_dir, char ***mass, int i, t_opt *res)
{
	DIR		*dir;
	int		l;
	char	**del;

	l = f_count_all(name_dir);
	mass = (char ***)malloc(sizeof(char **) * (i + 1));
	mass[i] = (char **)malloc(sizeof(char *) * (l + 1));
	del = mass[i];
	if (!(dir = opendir(name_dir)))
	{
		f_free_not(del, mass);
		return (0);
	}
	f_read_dir(dir, mass[i], name_dir);
	f_cheak_file(mass[i], res, l, name_dir);
	if (res->res_name == NULL)
	{
		i = f_rec_count(mass[i], mass, i, res);
		free(del);
	}
	else
		f_free_mass(l, mass[i]);
	free(mass);
	closedir(dir);
	return (i);
}

/*
//FUNCTION )))
*/

char	*FindFileByText(char* szStartDir, char* szFileMask, char* szTextPattern)
{
	t_opt	*res;
	int		i;

	i = 0;
	if (!(res = (t_opt *)malloc(sizeof(t_opt))))
		return (NULL);
	res->res_name = NULL;
	res->pattern = strdup(szTextPattern);
	res->name_file = strdup(szFileMask);
	f_rec_find(szStartDir, res->all_mass, i, res);
	if (res->res_name != NULL)
		return(res->res_name);
	else
	{
		res->res_name = strdup("No maches");
		return (res->res_name);
	}
}
